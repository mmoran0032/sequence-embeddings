""" data processing utilities
"""

from pathlib import Path
from typing import Dict, List, Tuple, Union

import numpy as np
import pandas as pd


def load_mind(data_path: Union[str, Path]) -> Dict[str, pd.DataFrame]:
    """load previously-downloaded MIND dataset

    Loads the `behaviors` and `news` datasets from Microsoft, with light
    processing on the `behaviors` set to create more readily usable
    information for downstream actions.
    """
    _path = Path(data_path)
    behaviors = (pd
        .read_parquet(_path / "behaviors.parquet")
        .pipe(process_data)
        .query("n_item >= 3")
    )
    news = pd.read_parquet("../data/news.parquet")
    return {"behaviors": behaviors, "news": news}


def process_data(df: pd.DataFrame) -> pd.DataFrame:
    return (df
        .assign(
            ts=lambda x: pd.to_datetime(x["time"]),
            dt=lambda x: x["ts"].dt.date.astype("datetime64[ns]"),
            arr_item=lambda x: x["history"].str.split(),
            n_item=lambda x: x["arr_item"].str.len(),
            impressions=lambda x: x["impressions"].str.split(),
            # I don't like using `apply()`, but here we are...
            arr_click=lambda x: x["impressions"].apply(filter_list, id_="-1"),
            n_click=lambda x: x["arr_click"].str.len(),
            arr_skip=lambda x: x["impressions"].apply(filter_list, id_="-0"),
            n_skip=lambda x: x["arr_skip"].str.len(),
        )
        .drop(columns=["time", "history", "impressions"])
        .fillna({"n_item": 0, "n_click": 0, "n_skip": 0})
        .astype({"n_item": int, "n_click": int, "n_skip": int})
    )


def filter_list(list_: List[str], id_: str = "-1") -> str:
    return [
        _i.rstrip(id_) for _i in list_ if _i.endswith(id_)
    ]


def prepare_sequences_basic(df: pd.DataFrame) -> Tuple[List[List[str]]]:
    """convert arrays into usable sequences

    We will take the item arrays from MIND and break it into the
    following parts:

    - [0, 1, ..., n-1] -> training data to generate our embeddings
    - [n] -> validation for training, where this item should be
      predicted by the previous item(s). used for hyperparameter
      optimization
    """
    arr_train = df["arr_item"].str.slice(stop=-1).tolist()
    arr_validation = df["arr_item"].str.slice(start=-1).tolist()
    return arr_train, arr_validation
