# Sequence embeddings

Predicting the next item in a sequence is useful in various contexts:
search and recommendations, retail, security, and other uses. Predicting
the next item in a sequence can be done in various ways, but usually
comes down to some level of, "what were the last item(s) that were
interacted with in recent history in some important way?"

Interactions can be:

- Clicks on items
- Purchases of items
- "Social" interaction (liking, commenting, etc.)
- Access or logging of an item

Taking these interactions, predicting the *next* item can be done in a
number of ways. One of the most powerful is building embeddings from the
sequences of item. If you consider sequence embeddings in the same way
as words in a sentence, then predicting the next word (for text
generation) a similar problem as the next *item* in a sequence. We can
create embeddings for items and use those to make predictions on the
next item.

## Dataset

The [MIND (Microsoft News Dataset)][1] ([paper][2]) is a large-scale
news recommendation dataset. Since news articles may only be relevant
for a short amount of time, and soon after they are published, the "cold
start" problem is very hard.

For this problem, we care about the `behaviors.tsv` file, which contains
the user activity, and the `news.tsv` file, which contains details on
the articles. For the second file, we really only care about the
category and sub-category as a way to assess some assumptions about the
produced embeddings (e.g. do articles cluster in category groupings?).

[1]: https://docs.microsoft.com/en-us/azure/open-datasets/dataset-microsoft-news
[2]: https://msnews.github.io/assets/doc/ACL2020_MIND.pdf
