from pathlib import Path
from typing import List

import pandas as pd

DATAPATH = Path() / ".." / "data"


def process_data() -> None:
    (pd
        .read_parquet(DATAPATH / "behaviors.parquet")
        .assign(
            ts=lambda x: pd.to_datetime(x["time"]),
            history=lambda x: x["history"].str.split(),
            impressions=lambda x: x["impressions"].str.split(),
            # I don't like using `apply()`, but here we are...
            clicks=lambda x: x["impressions"].apply(filter_list, id_="-1"),
            n_clicks=lambda x: x["clicks"].str.len(),
            skips=lambda x: x["impressions"].apply(filter_list, id_="-0"),
            n_skips=lambda x: x["skips"].str.len(),
        )
        .drop(columns=["time"])
        .to_parquet(DATAPATH / "behaviors-processed.parquet")
    )


def filter_list(list_: List[str], id_: str = "-1") -> str:
    return [
        _i.rstrip(id_) for _i in list_ if _i.endswith(id_)
    ]


if __name__ == "__main__":
    process_data()
