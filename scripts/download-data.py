""" download files

    copied from https://docs.microsoft.com/en-us/azure/open-datasets/dataset-microsoft-news
"""

import os
import shutil
import tempfile
import urllib.request
import zipfile

import pandas as pd


# Temporary folder for data we need during execution of this notebook (we'll clean up
# at the end, we promise)
temp_dir = os.path.join(tempfile.gettempdir(), "mind")
os.makedirs(temp_dir, exist_ok=True)


def download_url(
    url,
    destination_filename=None,
    progress_updater=None,
    force_download=False,
    verbose=True,
):
    """
    Download a URL to a temporary file
    """
    if not verbose:
        progress_updater = None
    # This is not intended to guarantee uniqueness, we just know it happens to guarantee
    # uniqueness for this application.
    if destination_filename is None:
        url_as_filename = url.replace("://", "_").replace("/", "_")
        destination_filename = os.path.join(temp_dir, url_as_filename)
    if (not force_download) and (os.path.isfile(destination_filename)):
        if verbose:
            print(
                "Bypassing download of already-downloaded file {}".format(
                    os.path.basename(url)
                )
            )
        return destination_filename
    if verbose:
        print(
            "Downloading file {} to {}".format(
                os.path.basename(url), destination_filename
            ),
            end="",
        )
    urllib.request.urlretrieve(url, destination_filename, progress_updater)
    assert os.path.isfile(destination_filename)
    nBytes = os.path.getsize(destination_filename)
    if verbose:
        print("...done, {} bytes.".format(nBytes))
    return destination_filename


base_url = "https://mind201910small.blob.core.windows.net/release"
training_small_url = f"{base_url}/MINDsmall_train.zip"
# validation_small_url = f"{base_url}/MINDsmall_dev.zip"
# training_large_url = f"{base_url}/MINDlarge_train.zip"
# validation_large_url = f"{base_url}/MINDlarge_dev.zip"

# For demonstration purpose, we will use small version validation set only.
# This file is about 30MB.
zip_path = download_url(training_small_url, verbose=True)
with zipfile.ZipFile(zip_path, "r") as zip_ref:
    zip_ref.extractall(temp_dir)

os.listdir(temp_dir)

# The behaviors.tsv file contains the impression logs and users' news click histories.
# It has 5 columns divided by the tab symbol:
# - Impression ID. The ID of an impression.
# - User ID. The anonymous ID of a user.
# - Time. The impression time with format "MM/DD/YYYY HH:MM:SS AM/PM".
# - History. The news click history (ID list of clicked news) of this user before this impression.
# - Impressions. List of news displayed in this impression and user's click behaviors on them (1 for click and 0 for non-click).
behaviors_path = os.path.join(temp_dir, "behaviors.tsv")
_df = pd.read_table(
    behaviors_path,
    header=None,
    names=["impression_id", "user_id", "time", "history", "impressions"],
)
_df.to_parquet("../data/behaviors.parquet")

# The news.tsv file contains the detailed information of news articles involved in the behaviors.tsv file.
# It has 7 columns, which are divided by the tab symbol:
# - News ID
# - Category
# - Subcategory
# - Title
# - Abstract
# - URL
# - Title Entities (entities contained in the title of this news)
# - Abstract Entities (entities contained in the abstract of this news)
news_path = os.path.join(temp_dir, "news.tsv")
_df = pd.read_table(
    news_path,
    header=None,
    names=[
        "id",
        "category",
        "subcategory",
        "title",
        "abstract",
        "url",
        "title_entities",
        "abstract_entities",
    ],
)
_df.loc[:, ["id", "category", "subcategory"]].to_parquet("../data/news.parquet")

shutil.rmtree(temp_dir)
